let cart = document.getElementById("cartItems");
document.onload = LoadShoppingCart();

function LoadShoppingCart(){
    let productIds = new Array();
    let total = 0;
    let currentProduct = cart.appendChild(document.createElement("div"));
    currentProduct.className = "cartItem";

    productIds = (sessionStorage.getItem("myCart")).split(",");

    for(let i = 0; i < productIds.length; i++){

        GetProductById(productIds[i])
        .then(data => {

            total = total + data[0].price;

            let currentProduct = cart.appendChild(document.createElement("div"));
            currentProduct.className = "cartItem";

            let productName = currentProduct.appendChild(document.createElement("i"));
            productName.innerHTML = data[0].name;

            let productPrice = currentProduct.appendChild(document.createElement("b"));
            productPrice.innerHTML = "€ " + data[0].price;

            

            if(i === productIds.length -1){
                cart.appendChild(document.createElement("hr"));
                let priceTag = cart.appendChild(document.createElement("b"));
                priceTag.innerHTML = "€ " + total;
                priceTag.style = "float: right;";
            }
        });

        
    }
}

function BuyItems(){
    console.log("items bought");
}