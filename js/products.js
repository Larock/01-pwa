let productContainer = document.getElementById("productContainer");
let promotions = document.getElementById("promotions");
document.onload = LoadProducts();


function LoadProducts(){
    GetProductsByCategory()
    .then(data=>{
        for(let i = 0; i < data.length; i++){
            let currentProduct = productContainer.appendChild(document.createElement("div"));
            currentProduct.className = "product";

            let productImg = currentProduct.appendChild(document.createElement("img"));
            productImg.src = data[i].imageURL;

            let productInfo = currentProduct.appendChild(document.createElement("div"));
            productInfo.style.borderTop = "2px solid black";

            let productName = productInfo.appendChild(document.createElement("p"));
            productName.innerHTML = data[i].name;

            let productPrice = productInfo.appendChild(document.createElement("p"));
            productPrice.innerHTML = "€ " + data[i].price;

            let addToCart = currentProduct.appendChild(document.createElement("div"));
            addToCart.className = "cover";

            let cartImg = addToCart.appendChild(document.createElement("img"));
            cartImg.src = "/img/shoppingcartgreen.png";
            cartImg.onclick = function(){

                let cartItems = new Array();

                if(sessionStorage.getItem("myCart")){
                    cartItems.push(sessionStorage.getItem("myCart"));
                    cartItems.push(data[i].id);

                    sessionStorage.setItem("myCart", cartItems);
                }
                else{
                    cartItems.push(JSON.stringify(data[i].id));

                    sessionStorage.setItem("myCart", cartItems);
                }
                alert("Toegevoegd aan winkelwagen");
            }
        }

        for(let i = 0; i < 2; i++){
            let currentProduct = promotions.appendChild(document.createElement("div"));
            currentProduct.className = "product";
            currentProduct.style.background = "white";

            let productImg = currentProduct.appendChild(document.createElement("img"));
            productImg.src = data[i].imageURL;

            let productInfo = currentProduct.appendChild(document.createElement("div"));
            productInfo.style.borderTop = "2px solid black";

            let productName = productInfo.appendChild(document.createElement("p"));
            productName.innerHTML = data[i].name;

            let productPrice = productInfo.appendChild(document.createElement("p"));
            productPrice.innerHTML = "€ " + data[i].price;

            let addToCart = currentProduct.appendChild(document.createElement("div"));
            addToCart.className = "cover";

            let cartImg = addToCart.appendChild(document.createElement("img"));
            cartImg.src = "/img/shoppingcartgreen.png";
            cartImg.onclick = function(){

                let cartItems = new Array();

                if(sessionStorage.getItem("myCart")){
                    cartItems.push(sessionStorage.getItem("myCart"));
                    cartItems.push(data[i].id);

                    sessionStorage.setItem("myCart", cartItems);
                }
                else{
                    cartItems.push(JSON.stringify(data[i].id));
                    
                    sessionStorage.setItem("myCart", cartItems);
                }
                alert("Toegevoegd aan winkelwagen");
            }
        }
    });
    
}