const AllProducts = 'http://localhost:3000/products/all';
const ProductsByCategory = 'http://localhost:3000/products/category/';
const ProductById = 'http://localhost:3000/products/id/';
let button = document.getElementById("btn");



function GetAllProducts(){
    fetch(AllProducts)
    .then(data=>{return data.json()})
    .then(res=>console.log(res));
}

async function GetProductsByCategory(){
    let response = await fetch(ProductsByCategory + document.title.toString());
    let data = await response.json();
    return data.results;
}

async function GetProductById(id){
    let response = await fetch(ProductById + id);
    let data = await response.json();
    return data.result;
}

function CreateOrder(){

}